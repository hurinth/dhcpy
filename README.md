# README #

In a Cisco Callmanager multi-cluster environment (typically a development or technical support scenario) were IP Phones need to be moved from one cluster to another, the configuration of the TFTP is done through the phone menu, which is slow and uncomfortable.

With DHCPy, users only have to access a Linux DHCP server and invoke the tool. From there, the pseudo GUI will let them edit the phones and reconfigure them so that the DHCP server will provide a new TFTP option, and will download files from it as soon as the ITL is removed.

DHCPy requires a CentOS 6.x or higher with dhcpd running.


### How do I get set up? ###

* Install dhcp package
* Add the following line to the sudoers file: 
* Configuration
